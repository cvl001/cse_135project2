package mainpackage;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.sql.Timestamp;
import java.util.Calendar;

public class DatabaseUtility {
	Connection conn = null;
	
	public DatabaseUtility() throws ClassNotFoundException, SQLException {
		begin();
	}
	
	public void updateTables() throws ClassNotFoundException, SQLException {
		ResultSet test = execute("SELECT current_database()");
		test.next();
		System.out.println(test.getString("current_database"));
		ResultSet new_sales = execute("select USERS.state,sales.pid,sales.quantity,sales.price from SALES INNER JOIN USERS ON USERS.id = SALES.uid where sales.timestamp > (SELECT * FROM last_updated)");
		
		while (new_sales.next()) {
			int new_sum = new_sales.getInt("price") * new_sales.getInt("quantity");
			int pid = new_sales.getInt("pid");
			int sid = new_sales.getInt("state");
			conn.createStatement().execute("UPDATE TOP_PID_VIEW SET sum = sum + " + new_sum + " WHERE pid = " + pid);
			conn.createStatement().execute("UPDATE TOP_STATES_VIEW SET sum = sum + " + new_sum + " WHERE state_id = " + sid);
			conn.createStatement().execute("UPDATE RATIOSUM SET sum = sum + " + new_sum + " WHERE state = " + sid + " AND pid = " + pid);
		}
		conn.createStatement().execute("UPDATE last_updated SET last = clock_timestamp();");
	}
	public ResultSet getChart() throws ClassNotFoundException, SQLException {
		String qry = "SELECT t2.*,t3.product_name,t3.sum as productsum FROM (" + 
"SELECT state as state_id,state_name,TOP_STATES_VIEW.sum as statesum,pid,RATIOSUM.sum as ratiosum FROM RATIOSUM INNER JOIN TOP_STATES_VIEW ON TOP_STATES_VIEW.state_id = RATIOSUM.state) t2 " +
"INNER JOIN (SELECT * FROM TOP_PID_VIEW order by sum desc limit 50) t3 ON t2.pid = t3.pid order by statesum desc, productsum desc, product_name;";
		return execute(qry);
	}
	
	public int getProductCount(int category) throws Exception {
		String qry;
		if(category == -1){
			qry = "SELECT COUNT(*) as cnt FROM PRODUCTS";
		}else{
			qry = "SELECT COUNT(*) as cnt FROM PRODUCTS WHERE CID = " + category + ";";
		}
		ResultSet res = execute(qry);
		
		if (res.next()) {
			return Integer.parseInt(res.getString("cnt"));
		} else {
			throw new Exception("Im confused");
		}
	}
	
	public int getRowCount(String row_type) throws Exception {
		if (row_type.equals("CUSTOMER")) {
			String qry = "SELECT COUNT(*) as cnt FROM USERS";
			ResultSet res = execute(qry);
			
			if (res.next()) {
				return Integer.parseInt(res.getString("cnt"));
			} else {
				throw new Exception("Im confused");
			}
		} else {
			return 50;
		}
	}
	
	public boolean checkForNextProducts(int page, int total) {
		return ((page+1) * 10) < total;
	}
	
	public boolean checkForNextCustomers(int page, int total) {
		return ((page+1) * 20) < total;
	}
	
	public ResultSet getCategoryNames() throws ClassNotFoundException, SQLException {
		String qry = "SELECT id,name FROM categories";
		return execute(qry);
	}
	
	
	public ResultSet getProductNames() throws ClassNotFoundException, SQLException {
		String qry = "select * from top_pid_view order by sum desc, product_name limit 50";
		return execute(qry);
	}
	
	public ResultSet getStates() throws ClassNotFoundException, SQLException {

		String qry = "SELECT * FROM top_states_view";
		return execute(qry);
	}
	public Connection begin() throws ClassNotFoundException, SQLException {	
		if (conn != null) {
			try {
				conn.close();
				conn = null;
			} catch (Exception e) {}
		}
		
	    Class.forName("org.postgresql.Driver"); 
//	    conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/largedb","postgres","admin");
	    conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/smalldb","postgres","admin");

	    //conn.setAutoCommit(false);
        return conn;
	}

	public void end() throws SQLException {
		conn.setAutoCommit(true);
		conn.close();
		conn = null;
	}
	
	public ResultSet execute(String qry) throws SQLException, ClassNotFoundException {
		Statement st = conn.createStatement();
		ResultSet res = st.executeQuery(qry);
		return res;
	}
	
	public ResultSet getProducts(int page) throws ClassNotFoundException, SQLException {
		int offset = page * 10;
		String qry = "SELECT name FROM PRODUCTS ORDER BY name LIMIT 10 OFFSET " + offset;
		return execute(qry);
	}
	
	public ResultSet getCustomerNames() throws ClassNotFoundException, SQLException {
		String qry = "SELECT name FROM users LIMIT 20 OFFSET 20";
		return execute(qry);
	}
	
	public ResultSet getChartData(int categoryId,String rowType, String sortType, int colPage, int rowPage) throws Exception{
		switch (rowType) {
		case "CUSTOMER": 
			if(sortType.equals("TOP-K")){
				return getCustomersTopKChart(categoryId,rowPage, colPage);
			} else if(sortType.equals("ALPHABETICAL")){
				return getCustomersAlphabeticalChart(categoryId,rowPage, colPage);
			}
		case "STATE" :
			if(sortType.equals("TOP-K")){
				return getStatesTopKChart(categoryId,rowPage, colPage);
			} else if(sortType.equals("ALPHABETICAL")){
				return getStatesAlphabeticalChart(categoryId,rowPage, colPage);
			}
			
		}
		return null;
	}
	private ResultSet getStatesAlphabeticalChart(int categoryId,int rowPage, int colPage) throws Exception {
		int product_offset = colPage * 10;
		int customer_offset = rowPage * 20;
		
		int l_offset = customer_offset + 1;
		int r_offset = l_offset + 19;
		String qry;
		if (categoryId == -1) {
			qry = 
				"WITH SALES_WITH_STATES AS (SELECT u1.state,SALES.pid,SALES.quantity,SALES.price FROM SALES INNER JOIN (SELECT * FROM USERS WHERE state BETWEEN "+ l_offset + " and "+ r_offset +") u1 ON SALES.uid = u1.id), " +
				"TOP_STATES AS (SELECT STATES.name,STATES.id as sid, TOTAL_STATE_SPENT FROM STATES INNER JOIN (SELECT state,SUM(quantity*price) as TOTAL_STATE_SPENT FROM SALES_WITH_STATES GROUP BY state) t2 ON STATES.id = t2.state), " +
				"TOP_PRODUCTS AS (SELECT name as product,pid,SUM(z1.quantity*z1.price) as total_product_bought FROM (SELECT * FROM SALES RIGHT JOIN (SELECT name,id FROM PRODUCTS ORDER BY name LIMIT 10 OFFSET "+ product_offset +") t1 ON t1.id = SALES.pid) z1 GROUP BY name,pid), " +
				"CROSS_TBL AS (SELECT * FROM TOP_STATES cross join TOP_PRODUCTS) " +
				"SELECT name as RowName,total_state_spent as RowTOTAL,product as ProdName,total_product_bought as ProdTOTAL,COALESCE(SUM(quantity*price),0) as ratioTotal FROM SALES_WITH_STATES s RIGHT JOIN CROSS_TBL crs ON crs.pid = s.pid AND crs.sid = s.state GROUP BY name,product,s.pid,total_state_spent,total_product_bought ORDER BY rowName,ProdName;"; 
		} else {
			qry = 
				"WITH PRODUCT_SALES AS(SELECT uid, s.pid, quantity, price, product FROM SALES s RIGHT JOIN (SELECT id as pid, name as product FROM PRODUCTS where cid = "+ categoryId + " order by name LIMIT 10 OFFSET "+ product_offset +" ) t1 ON t1.pid = s.pid) , " +
				"TOP_PRODUCTS AS(SELECT s.pid,s.product,SUM(quantity*price) as TOTAL_PRODUCT_BOUGHT FROM PRODUCT_SALES s group by s.pid, s.product), " +
				"SALES_WITH_STATES AS (SELECT u1.state,SALES.pid,SALES.quantity,SALES.price FROM PRODUCT_SALES As SALES RIGHT JOIN (SELECT * FROM USERS WHERE state BETWEEN "+ l_offset +" and " + r_offset + ") u1 ON SALES.uid = u1.id), " + 
				"TOP_STATES AS (SELECT STATES.name,STATES.id as sid, TOTAL_STATE_SPENT FROM STATES INNER JOIN (SELECT state,COALESCE(SUM(quantity*price),0) as TOTAL_STATE_SPENT FROM SALES_WITH_STATES GROUP BY state) t2 ON STATES.id = t2.state), " + 
				"CROSS_TBL AS (SELECT * FROM TOP_STATES cross join TOP_PRODUCTS) " +
				"SELECT name as RowName,total_state_spent as RowTOTAL,product as ProdName,total_product_bought as ProdTOTAL,COALESCE(SUM(quantity*price),0) as ratioTotal FROM SALES_WITH_STATES s RIGHT JOIN CROSS_TBL crs ON crs.pid = s.pid AND crs.sid = s.state GROUP BY name,product,s.pid,total_state_spent,total_product_bought ORDER BY rowName,ProdName;";		
		}
		return execute(qry);	
		}

	public ResultSet getCustomersAlphabeticalChart(int categoryId,int customer_page,int product_page) throws ClassNotFoundException, SQLException {
		System.out.println("ProductPage:" + product_page);
		System.out.println("CustomerPage:" + customer_page);
		int product_offset = product_page * 10;
		int customer_offset = customer_page * 20;
		String qry;
		
		if (categoryId == -1) {
			qry = 
				"WITH TOP_CUSTOMERS AS (SELECT t1.uid,t1.username as name,SUM(quantity*price) as total_row_spent FROM SALES s RIGHT JOIN (SELECT id as uid,name as username FROM USERS ORDER BY name asc LIMIT 20 OFFSET " + customer_offset + ") t1 on s.uid = t1.uid GROUP BY t1.uid,t1.username ORDER BY t1.username), " +
				"TOP_PRODUCTS AS (SELECT t1.pid,t1.product,SUM(quantity*price) as TOTAL_PRODUCT_BOUGHT FROM SALES s RIGHT JOIN (SELECT id as pid, name as product FROM PRODUCTS order by name LIMIT 10 OFFSET " + product_offset + ") t1 ON t1.pid = s.pid GROUP BY t1.pid,t1.product ORDER by t1.product), " +
				"CROSS_TBL AS (SELECT * FROM TOP_CUSTOMERS cross join TOP_PRODUCTS t2) " +
				"SELECT cx.name as RowName,cx.total_row_spent as RowTOTAL,cx.product as ProdName,cx.total_product_bought as prodTOTAL,COALESCE(SUM(QUANTITY*PRICE),0) as ratioTotal FROM SALES RIGHT JOIN CROSS_TBL cx ON cx.uid = SALES.uid AND cx.pid = SALES.pid GROUP BY cx.name,cx.product,cx.total_product_bought,total_row_spent ORDER BY name asc,product asc;";
		} else {
			qry = 
					"WITH PRODUCT_SALES AS(SELECT uid, s.pid, quantity, price, product FROM SALES s RIGHT JOIN (SELECT id as pid,  name as product FROM PRODUCTS where cid = "+ categoryId +" order by name LIMIT 10 OFFSET "+ product_offset +" ) t1 ON t1.pid = s.pid) , " +
		            "TOP_PRODUCTS AS(SELECT s.pid,s.product,SUM(quantity*price) as TOTAL_PRODUCT_BOUGHT FROM PRODUCT_SALES s group by s.pid, s.product), " +
		            "TOP_CUSTOMERS AS(SELECT t1.uid,t1.username as name,COALESCE(SUM(quantity*price), 0) as total_row_spent FROM PRODUCT_SALES s " +
		            "RIGHT JOIN (SELECT id as uid,name as username FROM USERS ORDER BY name asc LIMIT 20 OFFSET "+ customer_offset +") t1 on s.uid = t1.uid GROUP BY t1.uid,t1.username ORDER BY t1.username), " +
		            "CROSS_TBL AS (SELECT * FROM TOP_CUSTOMERS cross join TOP_PRODUCTS t2) " +
		            "SELECT cx.name as RowName,cx.total_row_spent as RowTOTAL,cx.product as ProdName,cx.total_product_bought as prodTOTAL,COALESCE(SUM(QUANTITY*PRICE),0) as ratioTotal FROM PRODUCT_SALES SALES RIGHT JOIN CROSS_TBL cx ON cx.uid = SALES.uid AND cx.pid = SALES.pid GROUP BY cx.name,cx.product,cx.total_product_bought,total_row_spent ORDER BY name asc,cx.product asc; ";
		}
		return execute(qry);
	}
	
	public HashMap<String,String> getCustomerTotals() throws ClassNotFoundException, SQLException {
		String qry = "SELECT DISTINCT user_name, SUM(total) as total FROM chartData WHERE product_name != '' group by user_name";
		ResultSet res = execute(qry);
		HashMap<String,String> customer_tots = new HashMap<String,String>();
		
		while (res.next()) {
			customer_tots.put(res.getString("user_name"),res.getString("total"));
		}
		
		return customer_tots;
	}
	
	public HashMap<String,String> getProductTotals() throws SQLException, ClassNotFoundException {
		String qry = "SELECT DISTINCT product_name,SUM(total) as total FROM chartData WHERE product_name != '' group by product_name";
		ResultSet res = execute(qry);
		HashMap<String,String> product_tots = new HashMap<String,String>();
		
		while (res.next()) {
			product_tots.put(res.getString("product_name"),res.getString("total"));
		}
		
		return product_tots;
	}
	
	/*---------------------------- Methods for TOP-K -------------------------------*/
	
	public ResultSet getStatesTopKChart(int categoryId,int rowPage, int colPage) throws ClassNotFoundException, SQLException {
		System.out.println("Getting STATES/TOPK data...");
		int rOffset = rowPage * 20;
		int cOffset = colPage * 10;
		String chartQuery;
		if (categoryId == -1) {
		chartQuery = "with state_sales as (select state, pid, (quantity * price) as ratio from sales " +
							"join (select id, state from users) u on u.id = sales.uid " +
							"group by state, pid, ratio), "+
							"ts as (select state, name as sname, sum(ratio) as stotal "+
							"from state_sales inner join states on states.id = state_sales.state "+
							"group by state, sname order by stotal desc limit 20 offset "+rOffset+"), "+
							"tp as ( select pid, pname, sum(quantity*sales.price) as ptotal " + 
							"from sales right join (select id, name as pname from products) p "+
							"on p.id = sales.pid group by pid, pname order by ptotal desc limit 10 offset " + cOffset +"), "+
							"combo as (select * from ts, tp) "+
							"select sname, stotal, pname, ptotal, coalesce(sum(ratio),0) as ratio from combo "+
							"left join state_sales ss on ss.state = combo.state and ss.pid = combo.pid "+
							"group by sname, stotal, pname, ptotal order by stotal desc, ptotal desc; ";
		} else  {
			chartQuery = 
					"WITH PRODUCT_SALES AS(SELECT uid, s.pid, quantity, price, product FROM SALES s RIGHT JOIN (SELECT id as pid, name as product FROM PRODUCTS where cid = "+categoryId+"  ) t1 ON t1.pid = s.pid) , " +
					"TOP_PRODUCTS AS(SELECT s.pid,s.product,SUM(quantity*price) as TOTAL_PRODUCT_BOUGHT FROM PRODUCT_SALES s group by s.pid, s.product order by  TOTAL_PRODUCT_BOUGHT desc LIMIT 10 offset "+cOffset+" ), " + 
					"SALES_WITH_STATES AS(SELECT u1.state,SALES.pid,SALES.quantity,SALES.price FROM PRODUCT_SALES As SALES RIGHT JOIN (SELECT * FROM USERS) u1 ON SALES.uid = u1.id), " +
					"TOP_STATES AS(SELECT STATES.name,STATES.id as sid, TOTAL_STATE_SPENT FROM STATES INNER JOIN (SELECT state, COALESCE(SUM(quantity*price), 0) as TOTAL_STATE_SPENT FROM SALES_WITH_STATES GROUP BY state) t2 ON STATES.id = t2.state order by TOTAL_STATE_SPENT desc limit 20 offset "+rOffset+"), " +
					"CROSS_TBL AS (SELECT * FROM TOP_STATES cross join TOP_PRODUCTS) " +
					"SELECT name as RowName,total_state_spent as RowTOTAL,product as ProdName,total_product_bought as ProdTOTAL,COALESCE(SUM(quantity*price),0) as ratioTotal FROM SALES_WITH_STATES s RIGHT JOIN CROSS_TBL crs ON crs.pid = s.pid AND crs.sid = s.state GROUP BY name,product,s.pid,total_state_spent,total_product_bought ORDER BY RowTOTAL desc, ProdTOTAL desc;";
		}
		
		return execute(chartQuery);
		
	}
	
	public ResultSet getCustomersTopKChart(int categoryId,int rowPage, int colPage) throws Exception {
		System.out.println("Getting CUSTOMERS/TOPK data...");
		int rOffset = rowPage * 20;
		int cOffset = colPage * 10;

		String chartQuery;
		
		if (categoryId == -1) {
			chartQuery = "with customer_sales as (select uid, pid, (quantity * price) as ratio from sales group by uid, pid, ratio), "+ // ratio for all users
					"tc as (select uid, name as uname, sum(ratio) as utotal from customer_sales right join users "+ // this is total user spent
					"on users.id = customer_sales.uid group by uid, uname order by utotal desc limit 20 offset "+rOffset+"), "+
					"tp as (select pid, pname, sum(quantity*sales.price) as ptotal from sales "+ 
					"right join (select id, name as pname from products) p on p.id = sales.pid "+
					"group by pid, pname order by ptotal desc limit 10 offset "+cOffset+") , "+
					"combo as (select * from tc, tp) "+
					"select uname, utotal, pname, ptotal, coalesce(sum(ratio),0) as ratio from combo "+
					"left join customer_sales cs on cs.uid = combo.uid and cs.pid = combo.pid "+
					"group by uname, utotal, pname, ptotal order by utotal desc, ptotal desc;";
		} else {
			chartQuery = 
					"WITH PRODUCT_SALES AS(SELECT uid, s.pid, quantity, price, product FROM SALES s RIGHT JOIN (SELECT id as pid, name as product FROM PRODUCTS where cid = "+categoryId+"  ) t1 ON t1.pid = s.pid), "+
					"TOP_PRODUCTS AS(SELECT s.pid,s.product,SUM(quantity*price) as TOTAL_PRODUCT_BOUGHT FROM PRODUCT_SALES s group by s.pid, s.product order by TOTAL_PRODUCT_BOUGHT desc LIMIT 10 OFFSET "+cOffset+"), " +
					"TOP_CUSTOMERS AS(SELECT t1.uid,t1.username as name,COALESCE(SUM(quantity*price), 0) as total_row_spent FROM PRODUCT_SALES s " +
					"RIGHT JOIN (SELECT id as uid,name as username FROM USERS ) t1 on s.uid = t1.uid GROUP BY t1.uid,t1.username ORDER by total_row_spent desc LIMIT 20 offset "+rOffset+"), " +
					"CROSS_TBL AS (SELECT * FROM TOP_CUSTOMERS cross join TOP_PRODUCTS t2) " +
					"SELECT cx.name as RowName,cx.total_row_spent as RowTOTAL,cx.product as ProdName,cx.total_product_bought as prodTOTAL,COALESCE(SUM(QUANTITY*PRICE),0) as ratioTotal FROM PRODUCT_SALES SALES " + 
					"RIGHT JOIN CROSS_TBL cx ON cx.uid = SALES.uid AND cx.pid = SALES.pid GROUP BY cx.name,cx.product,cx.total_product_bought,total_row_spent ORDER BY total_row_spent desc,rowname asc;"; 
		}
		return execute(chartQuery);
	}
	
	public ResultSet getRefreshData(String lastUpdated) throws Exception{
		
		String qry = "SELECT state,pid,sum(quantity*price) as total FROM sales WHERE timestamp > '" + lastUpdated + "' GROUP BY state,pid;";

		return execute(qry);
	}

}
