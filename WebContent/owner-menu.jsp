<div class="panel panel-default">
    <div class="panel-body">
        Menu :
        <div class="bottom-nav">
            <ul class="nav nav-list">
                <li><a href="categories.jsp" title="Categories">Categories</a></li>
                <li><a href="products.jsp" title="Products">Products</a></li>
                <li class="active"><a href="browsing.jsp" title="Browsing">Product Browsing</a></li>
                <li><a href="analytics.jsp" title="analytics">Analytics</a></li>
                <li><a href="purchase.jsp" title="purchase">Buy Shopping Cart</a></li>
            </ul>
        </div>
    </div>
</div>
