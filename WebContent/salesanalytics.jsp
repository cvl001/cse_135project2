<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ page language="java" import="java.sql.*"%>
        <%@ page language="java" import="java.util.*"%>
<%@page import="mainpackage.DatabaseUtility"%>
<%
	DatabaseUtility db = new DatabaseUtility();

	
	HashMap<String,Integer> products = new HashMap<String,Integer>();
	int productPageCount = 0;
	int customerPageCount = 0;
	int product_count = 0;
	int customer_count = 0;
	
	String sort_type = "ALPHABETICAL";
	String row_type = "CUSTOMER";
	int categoryId = -1;
	
	if (request.getParameter("row_type") != null)
		row_type = request.getParameter("row_type");
		
	if (request.getParameter("sort_type") != null)
		sort_type = request.getParameter("sort_type");
	
	if (request.getParameter("productPageCount") != null)
		productPageCount = Integer.parseInt(request.getParameter("productPageCount"));
	
	if (request.getParameter("customerPageCount") != null)
		customerPageCount = Integer.parseInt(request.getParameter("customerPageCount"));
	
	if (request.getParameter("category") != null)
		categoryId = Integer.parseInt(request.getParameter("category"));
	
	if (request.getAttribute("product_count") == null) {
		product_count = db.getProductCount(categoryId);
	} else {
		product_count = Integer.parseInt(request.getParameter("product_count"));
	}
	
	if (request.getAttribute("customer_count") == null) {
		customer_count = db.getRowCount(row_type);
	} else {
		customer_count = Integer.parseInt(request.getParameter("customer_count"));
	} 
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Sales Analytics</title>
</head>
<body>
	<form id="query_form">
	<%if (productPageCount == 0 && customerPageCount == 0) { %>
		<select name="category" form="query_form">
			<option value="-1">All</option>
			<% ResultSet categoryNames = db.getCategoryNames();
				while (categoryNames.next()) {
					int catId = categoryNames.getInt("id");
					String catName = categoryNames.getString("name");
			%>
			<option value="<%=catId%>"><%=catName%></option>
			<%} %>
		</select>
		<select name="row_type" form="query_form">
			<option value="CUSTOMER">Customers</option>
			<option value="STATE">States</option>
		</select>
		<select name="sort_type" form="query_form">
			<option value="ALPHABETICAL">Alphabetical</option>
			<option value="TOP-K">Top-K</option>
		</select>
		<br />
	<%} %>
	
		<br /><input type="submit" value="Run Query" />
		<br />
	</form>
	<% if (db.checkForNextProducts(productPageCount,product_count)) { %>
	<br />
	<form action="salesanalytics.jsp">
		<input type="hidden" value="<%=customerPageCount%>" name="customerPageCount"/>
		<input type="hidden" value="<%=productPageCount+1%>" name="productPageCount" />
		<input type="hidden" name="customer_count" value="<%=customer_count %>"/>
		<input type="hidden" name="product_count" value="<%=product_count %>"/>
		<input type="hidden" name="category" value="<%=categoryId %>" />
		<input type="hidden" value="<%=row_type%>" name="row_type" />
		<input type="hidden" value="<%=sort_type%>" name="sort_type" />
		<input type="submit" value="Next 10 Products"/>
	</form>
	<br />
	<% } %>
	<% if (db.checkForNextCustomers(customerPageCount,customer_count)) { %>
	<form action="salesanalytics.jsp">
		<input type="hidden" value="<%=customerPageCount+1%>" name="customerPageCount"/>
		<input type="hidden" value="<%=productPageCount%>" name="productPageCount" />
		<input type="hidden" name="customer_count" value="<%=customer_count %>"/>
		<input type="hidden" name="product_count" value="<%=product_count %>"/>
		<input type="hidden" name="category" value="<%=categoryId %>" />
		<input type="hidden" value="<%=row_type%>" name="row_type" />
		<input type="hidden" value="<%=sort_type%>" name="sort_type" />
		<input type="submit" value="Next 20 <%= (row_type.equals("STATE")?"States":"Customers") %>" />
	</form>	
	<br />
	<% } %>
	<table border="1" style="border-collapse:collapse; ">
		<tr>
			<td></td>
			<%
				System.out.println("BEFORE GETCHARTDATA");
				ResultSet chart = db.getChartData(categoryId,row_type, sort_type, productPageCount, customerPageCount);
			
				// Products : TotalSum
				LinkedHashMap<String, Integer> firstTenProducts = new LinkedHashMap<>();
				ArrayList<Integer> rowContent = new ArrayList<Integer>();
				String firstRowName = "";
				String firstProdName ="";
				String currProdName = null;
				int firstSum = 0;
				int prod_count = 0;
				boolean onlyOneRow = false;
				// Store the top Products
				chart.next();
				while(!firstProdName.equals(currProdName)){
					// First Row Name and Total Sum
					if(prod_count == 0){
						firstRowName = chart.getString(1);
						firstProdName = chart.getString(3);
						firstSum = chart.getInt(2);
					}

					// Header: Products : sum
					firstTenProducts.put(chart.getString(3), chart.getInt(4)); 
					
					// Row Content
					rowContent.add(Integer.parseInt(chart.getString(5)));
					
					prod_count++;					
					if(!chart.next()){
						onlyOneRow = true;
						break;
					}
					currProdName = chart.getString(3);
				}
				
				// Products
				for(Map.Entry<String, Integer> entry : firstTenProducts.entrySet()){ %>
						<td width="180px"><%= entry.getKey() %><br /><%="($ " + entry.getValue() + ")"%></td>
					
				<% } %>
		</tr>
		<!-- First Content Row -->
		<tr>
			<td width="280px"> <%= firstRowName %><br /><%="($ " + firstSum + ")" %></td>
			<% for(Integer i : rowContent){ %>
				<td>$ <%= i %></td>
			<% } %>
		</tr>
		
		<!-- Following Rows -->
		<%  int col = 0;
			boolean lastRow = false;
			for(int i = 0; i < 19; i++){ 
				if(onlyOneRow)
					break;%>
		<tr>	
			<% 	col = 0;
				while(col < prod_count){
					// Customer or State name
					if(col == 0) { %>
						<td width="280px"> <%= chart.getString(1) %><br /><%= "($ " + chart.getInt(2) + ")"%></td>
					<% } %>
					<!--  Row content -->		
					<td>$ <%= chart.getString(5) %></td>
			<% 		col++; 
					if(chart.next() == false)
						lastRow = true;
				} 
				if(lastRow == true)
					break;
				%>
		</tr>
		<% } %>
		
	</table>
</body>
</html>
<% db.end(); %>
