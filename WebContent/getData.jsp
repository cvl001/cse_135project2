<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ page language="java" import="java.sql.*"%>
        <%@ page language="java" import="java.util.*"%>
<%@page import="mainpackage.DatabaseUtility"%>

<%
	/******** REFRESH ***********/
	DatabaseUtility db = new DatabaseUtility();

	if (request.getParameter("action").equals("refresh")) {

		// Get data SINCE the last updated time
		ResultSet newData = db.getRefreshData(session.getAttribute("time").toString());	   	 
		
		// Update the time of last refresh to current time
		session.setAttribute("time", request.getParameter("time"));

		System.out.println("TIME: "+ session.getAttribute("time").toString());
	    
		
	    out.println("[ ");
		boolean afterfirst = false;
	    while(newData.next()){
	    	if(afterfirst)
				out.println(",");	
	    	
				out.println("{ \"id\": " + "\"s"+ newData.getInt("state") + "\" , \"total\" : " + newData.getInt("total") + " }, ");	
				out.println("{ \"id\": " + "\"p"+ newData.getInt("pid") + "\" , \"total\" : " + newData.getInt("total") + " }, ");	
				out.print("{ \"id\": " + "\"s"+ newData.getInt("state") + "p" + newData.getInt("pid") + "\" , \"total\" : " + newData.getInt("total") + " }");	
			
			afterfirst = true;
	    }
	    out.println("] ");
		
	} else {	
		//  RUN ACTION 		
%>
		<table border="1" style="border-collapse:collapse;">
<%
		// Set update time to now
		if(session.getAttribute("time") == null){
			session.setAttribute("time", request.getParameter("time"));
		}

		db.updateTables();
		ArrayList<String> productOrder = new ArrayList<String>();
		ResultSet products = db.getProductNames();
		//ResultSet states = db.getStates();
		ResultSet chart = db.getChart();
%>
	<tr>
		<td></td>
		<% while (products.next()) { 
			productOrder.add(products.getString("pid"));
		%>
		<td id="p<%=products.getString("pid")%>"><%=products.getString("product_name")%><br />$<%=products.getFloat("sum")%> </td>
		<% } %>
	</tr>
		<% 
			String current_state = "";
			int curr_idx = 500;
			while (chart.next()) { 

				String pid = chart.getString("pid");
				String sid = chart.getString("state_id");	
				
				if (!sid.equals(current_state)) {
					while (curr_idx < 50) {
						%><td id="s<%=current_state%>p<%=productOrder.get(curr_idx)%>">0</td><%	
						curr_idx++;
					}
					curr_idx = 0;
					
					if (!current_state.equals("")) {
						%></td><%
					}
					
					current_state = sid;
				%> 
				<tr>
				<td id="s<%=current_state%>"><%=chart.getString("state_name")%><br />$ <%=chart.getString("statesum") %> </td> 
				
				<%
				}

				while (curr_idx < 50 && !productOrder.get(curr_idx).equals(pid)) {
					%> <td id="s<%=current_state%>p<%=productOrder.get(curr_idx)%>">0</td> <%
					curr_idx++;
				}

				%><td id="s<%=current_state%>p<%=productOrder.get(curr_idx)%>" ><%=chart.getString("ratiosum")%></td> <%
				curr_idx++;				
		%>
		<% }%>
		<%
			if (curr_idx < 50) {
				%><td id="s<%=current_state%>p<%=productOrder.get(curr_idx)%>">0</td><%	
				curr_idx++;
			}
		%>
</table>

<% } %>

