function pad(number, length) {
   
    var str = '' + number;
    while (str.length < length) {
        str = '0' + str;
    }
   
    return str;

}

function getTime(){
	
	var dt = new Date();
	var dtstring = dt.getFullYear()
	    + '-' + pad(dt.getMonth()+1)
	    + '-' + pad(dt.getDate())
	    + ' ' + pad(dt.getHours())
	    + ':' + pad(dt.getMinutes())
	    + ':' + pad(dt.getSeconds())
		+ "." + pad(dt.getMilliseconds());
	return dtstring;
}

function transformCells(arr) {
    var i;
    for(i = 0; i < arr.length; i++) {
    	if(document.getElementById(arr[i].id) !== null){
    		var split = document.getElementById(arr[i].id).innerHTML.split("$");
    		if(split.length == 1){
    			document.getElementById(arr[i].id).innerHTML = parseInt(document.getElementById(arr[i].id).innerHTML, 10) + arr[i].total;
    		} else{
    			var newTotal = parseInt(split[1],10) + arr[i].total;
    			document.getElementById(arr[i].id).innerHTML = split[0] + "$ " + newTotal + "";
    		}
    			document.getElementById(arr[i].id).style.color = "red";
    	}
        //document.getElementsByTagName("table")[0].rows.item(document.getElementById(arr[i].id).parentNode.rowIndex).cells.item(0).style.color = "red";
        //document.getElementsByTagName("table")[0].rows.item(0).cells.item(document.getElementById(arr[i].id).cellIndex).style.color = "red";
    }
}

function refresh() {

	var params = [];
	params.push("action=refresh");
	params.push("time="+getTime());
	var url="getData.jsp?" + params.join("&");
	var xmlHttp = new XMLHttpRequest();
	
	xmlHttp.onreadystatechange = function() {
		if (xmlHttp.readyState != 4) {
			console.log(xmlHttp.readyState);
			return
		};
		
		var tds = document.getElementsByTagName("td");

	    for(var i = 0; i < tds.length; ++i)
	       tds[i].style.color = "black";	
	    
		console.log(xmlHttp.responseText);
		var dataArray = JSON.parse(xmlHttp.responseText);
		
		transformCells(dataArray);
	};
	
	xmlHttp.open("POST",url,true);
	xmlHttp.send(null);
}

function run() {
	var params = [];
	params.push("action=run");
	params.push("time="+getTime());
	
	var url="getData.jsp?" + params.join("&");
	var xmlHttp = new XMLHttpRequest();
	
	xmlHttp.onreadystatechange = function() {
		if (xmlHttp.readyState != 4) {
			console.log(xmlHttp.readyState);
			return
		};
		
		var tds = document.getElementsByTagName("td");

	    for(var i = 0; i < tds.length; ++i)
	       tds[i].style.color = "black";	
		
	    document.getElementById("chart").innerHTML = xmlHttp.responseText;
	};
	
	xmlHttp.open("POST",url,true);
	xmlHttp.send(null);
}

function signupUser() {

	if(!validateForm()) {
		var params = [];
		
		var name = document.forms["signup"]["name"].value;
	    var age = document.forms["signup"]["age"].value;
	    var role = document.forms["signup"]["role"].value;
	    var state = document.forms["signup"]["state"].value;
	    params.push(url);
	    params.push("name=" + name);
	    params.push("age=" + age);
	    params.push("role=" + role);
	    params.push("state=" + state);
	    console.log(params);
	    var xmlHttp = new XMLHttpRequest();
	    
	    xmlHttp.onreadystatechange=function() {
			if (xmlHttp.readyState != 4) {
				console.log(xmlHttp.readyState);
				return
			};
			
			if (xmlHttp.status != 200) {
				alert("HTTP status is " + xmlHttp.status + " instead of 200");
				return;
			};

			var responseDoc = xmlHttp.responseText;
			console.log(responseDoc);
			document.getElementById("msg").innerHTML = xmlHttp.responseText;
	
			//var response = eval('(' + responseDoc + ')');


		};
		var url="signupDB.jsp?" + params.join("&");
		console.log("THE URL" + url);
		xmlHttp.open("POST",url,true);
		xmlHttp.send(null);
	}
}

function validateForm() {
    var name = document.forms["signup"]["name"].value;
    var age = document.forms["signup"]["age"].value;
    
    var returnError = false;
    if (name == null || name == "") {
        document.getElementById('nameCheck').style.display = 'block';
		returnError = true;

    }
    else {
    	document.getElementById('nameCheck').style.display = 'none';
    }
    
    
	if( age == null || age == "") {
		document.getElementById('ageCheck').style.display = 'block';
		returnError = true;
    } 
	 else {
	    	document.getElementById('ageCheck').style.display = 'none';
	    }
	console.log(returnError);
    return returnError;
}
