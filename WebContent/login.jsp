<%@ page contentType="text/html; charset=utf-8" language="java"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<jsp:include page="head.html" />
</head>
<body class="page-index" data-spy="scroll" data-offset="60" data-target="#toc-scroll-target">
    <jsp:include page="header.jsp" />
    <div class="container">
        <div class="row">
            <div class="span12">
                <div class="body-content">
                    <div class="section">
                        <div class="page-header">
                            <h4>Log In</h4>
                        </div>
                        <div class="row">
                            <jsp:include page="login-form.html" />
                        </div>
                        <jsp:include page="footer.html" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
